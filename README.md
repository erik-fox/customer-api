# CRUD Customer API

### Customer creation endpoint

URI: /customers

Action: Create a customer

Status: CREATED

#### JSON Payload
```
{
    "id": 3,
    "firstName": "Rudy",
    "lastName": "Owen",
    "email": "rowen@gmail.com",
    "gender": "Male",
    "address": "642 7th St.",
    "city": "Huntington",
    "zip": 25701,
    "state": {
        "id": null,
        "abbreviation": "WV",
        "name": "West Virginia",
        "customerid": 3
    },
    "orders": [
        {
            "id": null,
            "productName": "Cell Phone",
            "itemCost": "250.59",
            "quantity": 1,
            "customerid": 3
        },
        {
            "id": null,
            "productName": "Scotch Tape",
            "itemCost": "3.49",
            "quantity": 10,
            "customerid": 3
        }
    ]
}
```

### Get all Customers endpoint

URI: /customers

Action: To get all current customers in the database

Status: OK

#### JSON Payload

```
[
...
    },
    {
        "id": 3,
        "firstName": "Rudy",
        "lastName": "Owen",
        "email": "rowen@gmail.com",
        "gender": "Male",
        "address": "642 7th St.",
        "city": "Huntington",
        "zip": 25701,
        "state": {
            "id": 1,
            "abbreviation": "WV",
            "name": "West Virginia",
            "customerid": 3
        },
        "orders": [
            {
                "id": 1,
                "productName": "Cell Phone",
                "itemCost": "250.59",
                "quantity": 1,
                "customerid": 3
            },
            {
                "id": 2,
                "productName": "Scotch Tape",
                "itemCost": "3.49",
                "quantity": 10,
                "customerid": 3
            }
        ]
    }
]
```

### Get Customer by id endpoint

URI: /customers/{id}

Action: Get a customer by a set id

Status: OK

#### JSON Payload

```
{
    "id": 3,
    "firstName": "Rudy",
    "lastName": "Owen",
    "email": "rowen@gmail.com",
    "gender": "Male",
    "address": "642 7th St.",
    "city": "Huntington",
    "zip": 25701,
    "state": {
        "id": 1,
        "abbreviation": "WV",
        "name": "West Virginia",
        "customerid": 3
    },
    "orders": [
        {
            "id": 1,
            "productName": "Cell Phone",
            "itemCost": "250.59",
            "quantity": 1,
            "customerid": 3
        },
        {
            "id": 2,
            "productName": "Scotch Tape",
            "itemCost": "3.49",
            "quantity": 10,
            "customerid": 3
        }
    ]
}
```

### Update Customer endpoint

URI: /customers/{id}

Action: Update a customer by a set id

Status: ACCEPTED

#### JSON Payload

```
{
    "id": 3,
    "firstName": "Amelia",
    "lastName": "Villa",
    "email": "avill@gmail.com",
    "gender": "Female",
    "address": "744 Van Nest Ave.",
    "city": "The Bronx",
    "zip": 10462,
    "state": {
        "id": null,
        "abbreviation": "NY",
        "name": "New York",
        "customerid": 5
    },
    "orders": [
        {
            "id": null,
            "productName": "Nail Clippers",
            "itemCost": "4.99",
            "quantity": 1,
            "customerid": 5
        }
    ]
}
```

### Delete a Customer endpoint

URI: /customers/{id}

Action: Delete a customer by a set id

Status: NO_CONTEXT

#### JSON Payload

```

```

### Create a Order endpoint

URI: /customers/{customerId}/orders

Action: Create a order attached to an already existing customer

Status: CREATED

#### JSON Payload

```
{
    "id": 5,
    "productName": "Granola Bars",
    "itemCost": "2.50",
    "quantity": 5,
    "customerid": 2
}
```

### Get all orders endpoint

URI: /orders

Action: Get all orders

Status: OK

#### JSON Payload

```
[
...
    },
    {
    "id": 5,
    "productName": "Granola Bars",
    "itemCost": "2.50",
    "quantity": 5,
    "customerid": 2
    }
]
```

### Get an Order by id

URI: /orders/{id}

Action: Get an existing order by a set id

#### JSON Payload

```
{
    "id": 5,
    "productName": "Granola Bars",
    "itemCost": "2.50",
    "quantity": 5,
    "customerid": 2
}
```

### Update an Order by id

URI: /orders/{id}

Action: Update an existing order by a set id

#### JSON Payload

```
{
    "id": 5,
    "productName": "Microphone",
    "itemCost": "149.99",
    "quantity": 1,
    "customerid": 2
}
```

### Delete an Order by id

URI: /orders/{id}

Action: Delete an existing order by a set id

#### JSON Payload

```

```
