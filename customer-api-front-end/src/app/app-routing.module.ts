import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ViewCustomerComponent } from './components/view-customer/view-customer.component';
import { AddCustomerComponent } from './components/add-customer/add-customer.component';
import { CustomersComponent } from './components/customers/customers.component';
import { EditCustomerComponent } from './components/edit-customer/edit-customer.component';
import { EditCustomerReactiveComponent } from './components/edit-customer-reactive/edit-customer-reactive.component';


const routes: Routes = [
  { path:'', redirectTo:'customer', pathMatch:'full' },
  { path:'customer', component: CustomersComponent },
  { path: 'edit/:id', component: EditCustomerReactiveComponent },
  { path:'add', component: AddCustomerComponent },
  { path:'view/:id', component: ViewCustomerComponent },
  { path:'**', redirectTo:'customer' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
