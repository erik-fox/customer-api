import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ICustomer } from 'src/app/shared/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-add-customer',
  templateUrl: './add-customer.component.html',
  styleUrls: ['./add-customer.component.css']
})
export class AddCustomerComponent implements OnInit {

  customer: ICustomer = {
    id: null,
    firstName: '',
    lastName: '',
    gender: null,
    address: '',
    email: '',
    city: '',
    zip: 0
    // state: {
    //   abbreviation: '',
    //   name: ''
    // },
    // orders:
    //   {
    //     productName: '',
    //     itemCost: 0,
    //     quantity: 0
    //   }
  };
  
  constructor(private router: Router, private dataServ: DataService) { }

  ngOnInit(): void {
  }

  submitCustomer() {
    this.dataServ.createCustomer(this.customer).subscribe(
      data => {
        console.log(data);
        this.router.navigate(['/']);
      }
    )
  }

  backToCustomerList() {
    this.router.navigate(['/']);
  }
}
