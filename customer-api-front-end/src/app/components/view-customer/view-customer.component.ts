import { Component, OnInit, Input } from '@angular/core';
import { ICustomer } from 'src/app/shared/interfaces';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-view-customer',
  templateUrl: './view-customer.component.html',
  styleUrls: ['./view-customer.component.css']
})
export class ViewCustomerComponent implements OnInit {

  customer: ICustomer = {
    id: null,
    firstName: '',
    lastName: '',
    gender: null,
    address: '',
    email: '',
    city: '',
    zip: 0
  };

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private dataServ: DataService
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params[`id`];
    this.dataServ.getCustomerById(id).subscribe(
      res => {
        this.customer = res;
      },
      err => console.log(err)
    );
  }

  goBackToList() {
    this.router.navigate(['/']);
  }

}
