import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../../services/data.service';
import { ICustomer } from '../../shared/interfaces';

@Component({
  selector: 'app-edit-customer',
  templateUrl: './edit-customer.component.html',
  styleUrls: ['./edit-customer.component.css']
})
export class EditCustomerComponent implements OnInit {

  // customer = new ICustomer();
  
  id: number;

  customer: ICustomer = {
    id: null,
    firstName: '',
    lastName: '',
    gender: null,
    address: '',
    email: '',
    city: '',
    zip: 0
    // state: {
    //   abbreviation: '',
    //   name: '',
    //   customerid: this.id
    // },
    // orders:
    //   {
    //     productName: '',
    //     itemCost: 0,
    //     quantity: 0,
    //     customerid: this.id
    //   }
  };

  constructor(
    private router: Router, 
    private service: DataService, 
    private activatedRoute: ActivatedRoute
    ) { }

  ngOnInit(): void {
    this.id = this.activatedRoute.snapshot.params[`id`];
    this.service.getCustomerById(this.id).subscribe(
      data => {
        this.customer = data;
      }
    );
  }

  editCustomerButton() {
    this.service.updateCustomer(this.customer, this.id).subscribe(
      res => {
        console.log(res);
        console.log('made it here');
        this.router.navigate(['customer']);
      },
      err => console.log(err)
    );
  }

  goBackToList(){
    this.router.navigate(['/'])
  }
}
