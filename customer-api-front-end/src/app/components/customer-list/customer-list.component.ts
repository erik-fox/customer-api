import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { DataService } from '../../services/data.service';
import { ICustomer } from 'src/app/shared/interfaces';

@Component({
  selector: 'customer-list',
  templateUrl: './customer-list.component.html',
  styleUrls: ['./customer-list.component.css']
})
export class CustomerListComponent implements OnInit {

  @Input() customers: ICustomer[] = [];

  // customers: ICustomer[] = [];

  constructor(private router: Router, private service: DataService) { }

  ngOnInit(): void {
    this.refreshCustomers();
  }


  refreshCustomers() {
    this.service.getCustomers().subscribe(
      data => {
        console.log(data);
        this.customers = data;
      }, 
      error => console.log(error)
    );
  }

  goToViewCustomer(id: number) {
    this.router.navigate(['view', id]);
  }

  goToEditCustomer(id: number) {
    this.router.navigate(['edit', id]);
  }

  deleteCustomer(id: number) {
    this.service.deleteCustomer(id).subscribe(
      res => {
        this.refreshCustomers();
      }
    )
  }

}
