import { Component, OnInit } from '@angular/core';
import { ICustomer } from 'src/app/shared/interfaces';
import { DataFilterService } from 'src/app/services/data-filter.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  title: string;
  customers: ICustomer[] = [];
  filteredCustomers: ICustomer[] = [];

  constructor(
    private router: Router,
    private dataFilter: DataFilterService
  ) { }

  ngOnInit(): void {
    this.title = 'Customers';
  }

  filterChanged(filterText: string) {
    if (filterText && this.customers) {
      let props = ['firstName', 'lastName', 'address', 'city', 'state.name'];
      this.filteredCustomers = this.dataFilter.filter(this.customers, props, filterText);
    } else {
      this.filteredCustomers = this.customers;
    }
  }

  // addCustomer() {
  //   this.router.navigate(['add']);
  // }

}
