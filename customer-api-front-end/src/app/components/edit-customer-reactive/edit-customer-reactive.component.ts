import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ICustomer, IState } from 'src/app/shared/interfaces';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from 'src/app/services/data.service';
import { ValidationService } from 'src/app/shared/validation.service';

@Component({
  selector: 'app-edit-customer-reactive',
  templateUrl: './edit-customer-reactive.component.html',
  styleUrls: ['./edit-customer-reactive.component.css']
})
export class EditCustomerReactiveComponent implements OnInit {

  customerForm: FormGroup;

  customer: ICustomer = {
    firstName: '',
    lastName: '',
    gender: '',
    address: '',
    email: '',
    city: '',
    zip: 0,
    state: {
      abbreviation: '',
      name: ''
    },
    orders: [
      {
        productName: '',
        itemCost: 0,
        quantity: 0
      }
    ]
  };

  states: IState[];

  errorMessage: string;

  deleteMessageEnabled: boolean;
  
  operationText: string = 'Create';

  constructor(
    private router: Router, 
    private route: ActivatedRoute, 
    private dataService: DataService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    if (id !== 0) {
      this.operationText = 'Update';
      this.getCustomer(id);
    }

    this.getStates();
    this.buildForm();
  }

  getCustomer(id: number) {
    this.dataService.getCustomerById(id)
      .subscribe((customer: ICustomer) => {
        this.customer = customer;
      },
      err => console.log(err)
      );
  }

  buildForm() {
    this.customerForm = this.fb.group({
      firstName:  [this.customer.firstName, Validators.required],
      lastName:   [this.customer.lastName, Validators.required],
      gender:     [this.customer.gender, Validators.required],
      email:      [this.customer.email, [Validators.required, ValidationService.emailValidator]],
      address:    [this.customer.address, Validators.required],
      city:       [this.customer.city, Validators.required],
      zip:        [this.customer.zip, Validators.required],
      states:     this.fb.group({
        abbreviation: ['', Validators.required],
        name:         ['', Validators.required],
        customerid:   [this.customer.id]
      }),
      orders:     this.fb.group({
        productName:  ['', Validators.required],
        itemCost:     ['', Validators.required],
        quantity:     ['', Validators.required],
        customerid:   [this.customer.id]
      })
    });
  }


  getStates() {
    this.dataService.getStates().subscribe(
      res => {
        this.states = res;
      }
    )
  }

  submit({value, valid}: {value: ICustomer, valid: boolean}) {
    
    value.id = this.customer.id;
    value.zip = this.customer.zip || 0;

    if (value.id) {
      console.log('In the if statement')
      this.dataService.updateCustomer(value, value.id).subscribe(
        res => {
          if (res) {
            this.router.navigate(['/customer']);
          } else {
            this.errorMessage = 'Unable to update customer';
          }
        },
        err => console.log(err)
      );
      
    } else {

      this.dataService.createCustomer(value).subscribe(
        res => {
          if (res) {
            this.router.navigate(['/customer']);
          } else {
            this.errorMessage = 'Unable to create customer';
          }
        },
        err => console.log(err)
      );

    }
  }

  cancel(event: Event) {
    event.preventDefault();
    this.router.navigate(['/']);
  }

  delete(event: Event) {
    event.preventDefault();
    this.dataService.deleteCustomer(this.customer.id).subscribe(
      res => {
        if (res) {
          this.router.navigate(['/']);
        } else {
          this.errorMessage = 'Unable to delete customer';
        }
      },
      err => console.log(err)
    );
  }

}
