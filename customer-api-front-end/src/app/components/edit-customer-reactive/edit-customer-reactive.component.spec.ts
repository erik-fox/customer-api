import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditCustomerReactiveComponent } from './edit-customer-reactive.component';
import { ReactiveFormsModule } from '@angular/forms';

describe('EditCustomerReactiveComponent', () => {
  let component: EditCustomerReactiveComponent;
  let fixture: ComponentFixture<EditCustomerReactiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCustomerReactiveComponent ],
      imports: [ReactiveFormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditCustomerReactiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
