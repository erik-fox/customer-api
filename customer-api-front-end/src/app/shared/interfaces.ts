export interface ICustomer {
    id?: number;
    firstName: string;
    lastName: string;
    email: string;
    address: string;
    city: string;
    state?: IState;
    stateId?: number;
    zip: number;
    gender: string;
    orderCount?: number;
    orders?: IOrders[];
    orderTotal?: number;
}

export interface IState {
    abbreviation?: string;
    name: string;
    customerid?: number;
}

export interface IOrders {
    productName: string;
    itemCost: number;
    quantity: number;
    customerid?: number;
    orderTotal?: number;
}
