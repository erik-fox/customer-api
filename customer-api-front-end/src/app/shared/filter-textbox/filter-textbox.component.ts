import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-filter-textbox',
  template: `
    <form>
        Filter:
        <input type="text" name="filter"
               [(ngModel)]="model.filter"
               (keyup)="filterChanged($event)">
    </form>
  `
})
export class FilterTextboxComponent implements OnInit {

  model: { filter: string } = { filter: null };

  @Output()
  changed: EventEmitter<string> = new EventEmitter<string>(); 

  constructor() { }

  ngOnInit(): void {
  }

  filterChanged(event: any) {
    event.preventDefault();
    this.changed.emit(this.model.filter);
  }

}
