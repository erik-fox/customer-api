import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ICustomer, IState } from '../shared/interfaces';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  baseUrl: string = 'http://localhost:8080/customers';

  baseStatesUrl: string = 'http://localhost:8080/states';

  constructor(private http: HttpClient) { }

  getCustomers(): Observable<ICustomer[]> {
    return this.http.get<ICustomer[]>(this.baseUrl);
  }

  getCustomerById(id: number): Observable<ICustomer> {
    return this.http.get<ICustomer>(`${this.baseUrl}/${id}`);
  }
  
  createCustomer(customer: ICustomer): Observable<ICustomer> {
    return this.http.post<ICustomer>(this.baseUrl, customer);
  }

  updateCustomer(customer: ICustomer, id: number): Observable<ICustomer> {
    console.log('I am here')
    return this.http.put<ICustomer>(`${this.baseUrl}/${id}`, customer);
  }

  deleteCustomer(id: number): Observable<any> {
    return this.http.delete<any>(`${this.baseUrl}/${id}`);
  }

  getStates(): Observable<IState[]> {
    return this.http.get<IState[]>(this.baseStatesUrl);
  }

  // calculateCustomersOrderTotal(customers: ICustomer[]) {
  //   for (let customer of customers) {
  //     if (customer && customer.orders) {
  //       let total = 0;
  //       for (let order of customer.orders) {
  //         total += (order.itemCost * order.quantity);
  //       }
  //       customer.orderTotal = total;
  //     }
  //   }
  // }
}
