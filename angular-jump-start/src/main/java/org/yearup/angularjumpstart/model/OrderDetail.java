//package org.yearup.angularjumpstart.model;
//
//import com.fasterxml.jackson.annotation.JsonIdentityInfo;
//import com.fasterxml.jackson.annotation.ObjectIdGenerators;
//
//import javax.persistence.*;
//
//@Entity
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "id")
//public class OrderDetail {
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private Long id;
//
//    private String productName;
//    private String itemCost;
//    private int quantity;
//
//    @ManyToOne
//    @JoinColumn(name = "orderid", insertable = false, updatable = false)
//    private Orders orders;
//
//    private Long orderid;
//
//    public OrderDetail() {
//    }
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    public String getProductName() {
//        return productName;
//    }
//
//    public void setProductName(String productName) {
//        this.productName = productName;
//    }
//
//    public String getItemCost() {
//        return itemCost;
//    }
//
//    public void setItemCost(String itemCost) {
//        this.itemCost = itemCost;
//    }
//
//    public int getQuantity() {
//        return quantity;
//    }
//
//    public void setQuantity(int quantity) {
//        this.quantity = quantity;
//    }
//
//    public Orders getOrders() {
//        return orders;
//    }
//
//    public Long getOrderid() {
//        return orderid;
//    }
//
//    public void setOrderid(Long orderid) {
//        this.orderid = orderid;
//    }
//
//    @Override
//    public String toString() {
//        return "OrderDetail{" +
//                "id=" + id +
//                ", productName='" + productName + '\'' +
//                ", itemCost='" + itemCost + '\'' +
//                ", quantity=" + quantity +
//                ", orders=" + orders +
//                ", orderid=" + orderid +
//                '}';
//    }
//}
