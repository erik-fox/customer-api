package org.yearup.angularjumpstart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.yearup.angularjumpstart.model.Customer;

import java.util.Date;
import java.util.List;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Long> {

//    @Query("from Customer where purchaseDate=!purchaseDate, and currentDate=!currentDate")
//    List<Customer> findCustomers (Date from, Date to);

}
