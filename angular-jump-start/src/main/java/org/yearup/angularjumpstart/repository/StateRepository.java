package org.yearup.angularjumpstart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.yearup.angularjumpstart.model.State;

public interface StateRepository extends JpaRepository<State, Long> {
}
