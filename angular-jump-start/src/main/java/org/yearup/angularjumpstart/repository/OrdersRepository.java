package org.yearup.angularjumpstart.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.yearup.angularjumpstart.model.Orders;

public interface OrdersRepository extends JpaRepository<Orders, Long> {
}
