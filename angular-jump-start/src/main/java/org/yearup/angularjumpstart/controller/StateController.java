package org.yearup.angularjumpstart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.yearup.angularjumpstart.model.State;
import org.yearup.angularjumpstart.service.StateService;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class StateController {

    @Autowired
    private StateService stateService;

    @RequestMapping(value = "/states", method = RequestMethod.GET)
    public ResponseEntity<List<State>> getStates() {
        List<State> stateList = stateService.getStates();
        return new ResponseEntity<>(stateList, HttpStatus.OK);
    }

}
