package org.yearup.angularjumpstart.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.yearup.angularjumpstart.model.Orders;
import org.yearup.angularjumpstart.service.OrdersService;

import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
public class OrdersController {

    @Autowired
    private OrdersService ordersService;

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public ResponseEntity<List<Orders>> getAllOrders() {
        List<Orders> orders = ordersService.getAllOrders();
        return new ResponseEntity<>(orders, HttpStatus.OK);
    }

    @RequestMapping(value = "/orders/{id}", method = RequestMethod.GET)
    public ResponseEntity<Optional<Orders>> getOrderById(@PathVariable Long id) {
        Optional<Orders> order = ordersService.getOrderById(id);
        return new ResponseEntity<>(order, HttpStatus.OK);
    }

    @RequestMapping(value = "/customers/{customerId}/orders", method = RequestMethod.POST)
    public ResponseEntity<?> createOrder(@RequestBody Orders orders, @PathVariable Long customerId) {
        ordersService.createOrder(orders, customerId);
        HttpHeaders httpHeaders = new HttpHeaders();
        URI newCustomerUri = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/orders")
                .buildAndExpand(orders.getId())
                .toUri();
        httpHeaders.setLocation(newCustomerUri);
        return new ResponseEntity<>(orders, httpHeaders, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/orders/{id}", method = RequestMethod.PUT)
    public ResponseEntity<?> updateOrder(@PathVariable Long id, @RequestBody Orders orders) {
        ordersService.updateOrder(id, orders);
        return new ResponseEntity<>(orders, HttpStatus.ACCEPTED);
    }

    @RequestMapping(value = "/orders/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteOrderById(@PathVariable Long id) {
        ordersService.deleteOrderById(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
