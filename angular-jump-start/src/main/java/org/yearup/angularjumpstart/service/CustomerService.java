package org.yearup.angularjumpstart.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yearup.angularjumpstart.exception.ResourceNotFoundException;
import org.yearup.angularjumpstart.model.Customer;
import org.yearup.angularjumpstart.repository.CustomerRepository;

import java.util.*;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;

    public void verifyCustomer(Long id) throws ResourceNotFoundException {
        Optional<Customer> customer = customerRepository.findById(id);
        if (customer.isEmpty()) {
            throw new ResourceNotFoundException("Customer with id " + id + " not found.");
        }
    }

    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    public Optional<Customer> getCustomerById(Long id) {
        verifyCustomer(id);
        return customerRepository.findById(id);
    }

    public void createCustomer(Customer customer) {
        customerRepository.save(customer);
    }

    public void updateCustomer(Long id, Customer customer) {
//        verifyCustomer(id);
        for (Customer customer1: customerRepository.findAll()) {
            if (id.equals(customer.getId())) {
                customerRepository.save(customer);
            }
        }

    }

    public void deleteCustomerById(Long id) {
        verifyCustomer(id);
        customerRepository.deleteById(id);
    }

}

