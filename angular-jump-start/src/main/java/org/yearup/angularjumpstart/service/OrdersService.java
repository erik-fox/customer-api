package org.yearup.angularjumpstart.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yearup.angularjumpstart.exception.ResourceNotFoundException;
import org.yearup.angularjumpstart.model.Customer;
import org.yearup.angularjumpstart.model.Orders;
import org.yearup.angularjumpstart.repository.CustomerRepository;
import org.yearup.angularjumpstart.repository.OrdersRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class OrdersService {

    @Autowired
    private OrdersRepository ordersRepository;

    @Autowired
    private CustomerRepository customerRepository;

    public void verifyCustomer(Long id) throws ResourceNotFoundException {
        Optional<Customer> customer = customerRepository.findById(id);
        if (customer.isEmpty()) {
            throw new ResourceNotFoundException("Customer with id " + id + " not found.");
        }
    }

    public void verifyOrder(Long id) throws ResourceNotFoundException {
        Optional<Orders> orders = ordersRepository.findById(id);
        if (orders.isEmpty()) {
            throw new ResourceNotFoundException("Order with id " + id + " not found.");
        }
    }

    public List<Orders> getAllOrders() {
        return new ArrayList<>(ordersRepository.findAll());
    }

    public Optional<Orders> getOrderById(Long orderId) {
        verifyOrder(orderId);
        return ordersRepository.findById(orderId);
    }

    public void createOrder(Orders orders, Long customerId) {
        verifyCustomer(customerId);
        for (Customer customer : customerRepository.findAll()){
            if (customer.getId().equals(customerId)){
                orders.setCustomer(customer);
                orders.setCustomerid(customerId);
                ordersRepository.save(orders);
            }
        }
    }

    public void updateOrder(Long orderId, Orders orders) {
        verifyOrder(orderId);
        for (Customer customer : customerRepository.findAll()){
            orders.setCustomer(customer);
            for (Orders orders1 : ordersRepository.findAll()){
                if (orders1.getId().equals(orderId)){
                    ordersRepository.save(orders);
                }
            }
        }
    }

    public void deleteOrderById(Long orderId) {
        verifyOrder(orderId);
        ordersRepository.deleteById(orderId);
    }

}
